package jUnit;

import com.serenity.bdd.businessRules.BusinessRules;
import org.junit.Assert;
import org.junit.Test;

public class testBusinessRules {

    private BusinessRules businessRules = new BusinessRules();

    @Test
    public void  makeAPredictionTest(){
        Assert.assertEquals(2,businessRules.makeAPrediction("1.05","2.33","1.03"));
    }

}
