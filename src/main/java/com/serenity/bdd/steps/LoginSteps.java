package com.serenity.bdd.steps;

import com.serenity.bdd.pages.DashboardPage;
import com.serenity.bdd.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.jbehave.core.model.ExamplesTable;

public class LoginSteps extends ScenarioSteps {

    @Page
    private LoginPage loginPage;

    @Page
    private DashboardPage dashboardPage;

    @Step
    public void openPage(){
        loginPage.open();
    }

    @Step
    public void goToLogin(){
        loginPage.goToLogin();
    }

    @Step
    public void login(ExamplesTable loginData){
        loginPage.login(loginData);
    }

    @Step
    public void validateIfDashboard(){
        dashboardPage.validateIfDashboard();
    }
}