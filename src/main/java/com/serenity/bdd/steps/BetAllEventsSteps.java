package com.serenity.bdd.steps;

import com.serenity.bdd.businessRules.BusinessRules;
import com.serenity.bdd.pages.BetPage;
import com.serenity.bdd.pages.DashboardPage;
import com.serenity.bdd.pages.EventsPage;
import com.serenity.bdd.pages.MenuPage;
import net.thucydides.core.annotations.Step;
import org.fluentlenium.core.annotation.Page;
import org.jbehave.core.model.ExamplesTable;

public class BetAllEventsSteps {
    private BusinessRules businessRules = new BusinessRules();

    @Page
    private DashboardPage dashboardPage;

    @Page
    private EventsPage eventsPage;

    @Page
    private BetPage betPage;

    @Page
    private MenuPage menuPage;

    @Step
    public void betInAllEvents(ExamplesTable betData){
        for (int i=1;i<=2;i++){
                menuPage.goToEvents();
                eventsPage.validateIfEvents();
                eventsPage.selectEventToBet(1,i);
                int side = betPage.selectSide();
                betPage.makeABet(betData,side);
                betPage.validateIfBet(side);
        }

        for (int i=2;i<=3;i++){
            for (int j=1;j<=3;j++) {
                menuPage.goToEvents();
                eventsPage.validateIfEvents();
                eventsPage.selectEventToBet(i, j);
                int side = betPage.selectSide();
                betPage.makeABet(betData, side);
                betPage.validateIfBet(side);
            }
        }

        for (int i=1;i<=6;i++){
            menuPage.goToEvents();
            eventsPage.validateIfEvents();
            eventsPage.selectEventToBet(4,i);
            int side = betPage.selectSide();
            betPage.makeABet(betData,side);
            betPage.validateIfBet(side);
        }

    }
}
