package com.serenity.bdd.steps;

import com.serenity.bdd.pages.SerenityHomePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;

/**
 * Created by Lenovo on 11/04/2017.
 */
public class SmokeTestSteps extends ScenarioSteps{
    @Page
    private SerenityHomePage serenityHomePage;

    @Step
    public void openPage(){
        serenityHomePage.open();
    }

    @Step
    public void waitTitle(){
        serenityHomePage.waitTitle();
    }

    @Step
    public void clickTitle(){
        serenityHomePage.clickTitle();
    }
}
