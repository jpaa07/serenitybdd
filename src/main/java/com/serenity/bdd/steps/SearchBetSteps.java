package com.serenity.bdd.steps;

import com.serenity.bdd.pages.BetPage;
import com.serenity.bdd.pages.DashboardPage;
import com.serenity.bdd.pages.EventsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.jbehave.core.model.ExamplesTable;

public class SearchBetSteps extends ScenarioSteps {
    @Page
    private DashboardPage dashboardPage;

    @Page
    private EventsPage eventsPage;

    @Page
    private BetPage betPage;

    @Step
    public void validateIfDashboard(){
        dashboardPage.validateIfDashboard();
    }

    @Step
    public void searchTeam(ExamplesTable searchData){
        dashboardPage.searchTeam(searchData);
    }

    @Step
    public void selectEvent(){
        eventsPage.selectEvent();
    }

    @Step
    public void selectSide(){
        betPage.selectSide();
    }

    @Step
    public void makeABet(ExamplesTable betData){
        betPage.makeABet(betData,1);
    }

    @Step
    public void validateIfBet(){
        betPage.validateIfBet(1);
    }
}
