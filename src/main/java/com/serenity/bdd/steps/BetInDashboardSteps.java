package com.serenity.bdd.steps;

import com.serenity.bdd.businessRules.BusinessRules;
import com.serenity.bdd.pages.BetPage;
import com.serenity.bdd.pages.DashboardPage;
import com.serenity.bdd.pages.EventsPage;
import com.serenity.bdd.pages.MenuPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;
import org.jbehave.core.model.ExamplesTable;


public class BetInDashboardSteps extends ScenarioSteps {

    private BusinessRules businessRules = new BusinessRules();

    @Page
    private DashboardPage dashboardPage;

    @Page
    private EventsPage eventsPage;

    @Page
    private BetPage betPage;

    @Page
    private MenuPage menuPage;

    @Step
    public void makeNBets(ExamplesTable betData){
        for (int i = 1; i <= 6; i++){
            businessRules.waitSeconds(1);
            dashboardPage.validateIfDashboard();
            dashboardPage.selectDashboardEvent(i);
            int side = betPage.selectSide();
            betPage.makeABet(betData,side);
            betPage.validateIfBet(side);
            menuPage.goToDashboard();
            businessRules.waitSeconds(1);
        }
    }
}

