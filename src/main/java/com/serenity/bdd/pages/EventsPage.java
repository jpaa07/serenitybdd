package com.serenity.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.MatcherAssert;
import org.openqa.selenium.By;

public class EventsPage extends PageObject {

    private static String EVENTPATH = "html/body/div[1]/section/section/section[2]/div/section/div[2]/ul[VALUE1]/li[VALUE2]//a";

    @FindBy(xpath = "html/body/div[1]/section/section/section[2]/div/section/div[2]/ul[1]/li[1]//a")
    private WebElementFacade linkFirstSearchEvent;

    public void selectEvent(){
        linkFirstSearchEvent.waitUntilVisible();
        linkFirstSearchEvent.click();
    }

    public void selectEventToBet(int ul, int li){
        String strUl = "" + ul;
        String strLi = "" + li;
        String strEventElement = EVENTPATH.replace("VALUE1",strUl);
        strEventElement = strEventElement.replace("VALUE2",strLi);
        WebElementFacade eventElement = element(By.xpath(strEventElement));
        eventElement.click();
    }

    public void validateIfEvents(){
        linkFirstSearchEvent.waitUntilVisible();
        MatcherAssert.assertThat("the page didn't load",linkFirstSearchEvent.isVisible());
    }
}
