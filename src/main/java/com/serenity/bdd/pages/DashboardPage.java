package com.serenity.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.hamcrest.MatcherAssert;
import org.jbehave.core.model.ExamplesTable;
import org.openqa.selenium.By;

import java.util.Map;

/**
 * Created by Lenovo on 11/04/2017.
 */
public class DashboardPage extends PageObject {

    private final static String DASHBOARDEVENTPATH = "html/body/div[1]/section/section/section[2]/div/div[1]/div[1]/div/article/article//li[ROW]";

    @FindBy(className = "js-dashboard")
    private WebElementFacade dashboardSection;

    @FindBy(xpath = "html/body/div[1]/section/nav/section/div/ul/li[1]/a")
    private WebElementFacade buttonSearch;

    @FindBy(xpath = ".//*[@id='nav-search']")
    private WebElementFacade textSearch;

    public void validateIfDashboard(){
        dashboardSection.waitUntilVisible();
        MatcherAssert.assertThat("Dashboard is not visible",dashboardSection.isVisible());
    }

    public void searchTeam(ExamplesTable searchData){
        Map<String,String> searchDataRow = searchData.getRow(0);
        buttonSearch.click();
        textSearch.typeAndEnter(searchDataRow.get("team"));
    }

    public void selectDashboardEvent(int row){
        String strRow = row + "";
        String dashboardEvent = DASHBOARDEVENTPATH.replace("ROW",strRow);
        WebElementFacade linkDashEvent = element(By.xpath(dashboardEvent));
        linkDashEvent.click();
    }
}
