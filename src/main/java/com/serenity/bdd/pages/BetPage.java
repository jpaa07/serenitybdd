package com.serenity.bdd.pages;

import com.serenity.bdd.businessRules.BusinessRules;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.fluentlenium.core.annotation.Page;
import org.hamcrest.MatcherAssert;
import org.jbehave.core.model.ExamplesTable;

import java.util.Map;

public class BetPage extends PageObject {
    private BusinessRules businessRules = new BusinessRules();
    private static final String TEAMSIDEPATH = "html/body/div[1]/section/section/section/div/div/article[1]/div[1]/div//li";


    @FindBy(xpath = TEAMSIDEPATH + "[1]/a")
    private WebElementFacade btnBetToHome;

    @FindBy(xpath = TEAMSIDEPATH + "[2]/a")
    private WebElementFacade btnBetToTie;

    @FindBy(xpath = TEAMSIDEPATH + "[3]/a")
    private WebElementFacade btnBetToVisit;

    @FindBy(xpath = TEAMSIDEPATH + "[1]//span[2]")
    private WebElementFacade betValueHome;

    @FindBy(xpath = TEAMSIDEPATH + "[2]//span[2]")
    private WebElementFacade betValueTie;

    @FindBy(xpath = TEAMSIDEPATH + "[3]//span[2]")
    private WebElementFacade betValueVisit;

    @FindBy(xpath = ".//*[@id='points']")
    private WebElementFacade textPoints;

    @FindBy(xpath = ".//*[@id='play-action']")
    private WebElementFacade buttonBet;

    @FindBy(xpath = "html/body/div[1]/section/section/section[2]/div/div/article[1]/ul/li")
    private WebElementFacade divValidationBet;

    public int selectSide(){
        String homeValue, tieValue, visitValue;
        btnBetToHome.waitUntilEnabled();
        homeValue = betValueHome.getText();
        tieValue = betValueTie.getText();
        if(betValueVisit.isPresent())
            visitValue = betValueVisit.getText();
        else
            visitValue = "99.9";
        int prediction = businessRules.makeAPrediction(homeValue,tieValue,visitValue);
        switch (prediction) {
            case 1:
                btnBetToHome.click();
                break;
            case 2:
                btnBetToTie.click();
                break;
            case 3:
                btnBetToVisit.click();
                break;
            case 0:
                return 0;
        }
        return 1;
    }

    public void makeABet(ExamplesTable betData, int value){
        switch (value){
            case 1:
                Map<String,String> betDataRow = betData.getRow(0);
                textPoints.waitUntilEnabled();
                textPoints.clear();
                textPoints.sendKeys(betDataRow.get("coins"));
                buttonBet.click();
                break;
            case 0:
                break;
        }
    }

    public void validateIfBet(int value){
        switch (value) {
            case 1:
                divValidationBet.waitUntilVisible();
                MatcherAssert.assertThat("Not bet found",divValidationBet.isVisible());
                break;
            case 0:
                break;
        }
    }
}
