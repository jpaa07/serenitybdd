package com.serenity.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class MenuPage extends PageObject {
    @FindBy(xpath = "html/body/div[1]/aside/nav/ul/li[1]/a")
    private WebElementFacade buttonDashboard;

    @FindBy(xpath = "html/body/div[1]/aside/nav/ul/li[2]/a")
    private WebElementFacade buttonEvents;

    public void goToDashboard(){
        buttonDashboard.waitUntilVisible().click();
    }

    public void goToEvents(){
        buttonEvents.waitUntilVisible().click();
    }
}
