package com.serenity.bdd.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;


@DefaultUrl("https://github.com/serenity-bdd")
public class SerenityHomePage extends PageObject {
    @FindBy(xpath = ".//div/header/div/div/div/h1")
    WebElementFacade labelSerenityTitle;

    public SerenityHomePage(WebDriver webDriver){
        super(webDriver);
    }

    public void waitTitle(){
        labelSerenityTitle.waitUntilVisible();
    }

    public void clickTitle(){
        labelSerenityTitle.click();
    }
}
