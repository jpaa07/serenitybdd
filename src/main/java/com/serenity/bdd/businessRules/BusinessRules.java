package com.serenity.bdd.businessRules;

public class BusinessRules {

    public int makeAPrediction(String homeValue, String tieValue, String visitValue){

        double home = getStringToDecimal(homeValue);
        double tie = getStringToDecimal(tieValue);
        double visit = getStringToDecimal(visitValue);

        if(isLowerNumber(home,tie,visit)){
            if(validateProbability(home,tie,visit))
                return 1;
        } else if (isLowerNumber(tie,home,visit)){
            if(validateProbability(tie,home,visit))
                return 2;
        } else if (isLowerNumber(visit,home,tie)){
            if(validateProbability(visit,home,tie))
                return 3;
        }
        return 0;
    }

    private double getStringToDecimal(String value){
        String intPart = value.substring(0,value.indexOf("."));
        String decimalPart = value.substring(value.indexOf(".")+1);
        int integer = Integer.parseInt(intPart);
        int intDecimal = Integer.parseInt(decimalPart);
        double doubleDecimal = getDecimalPart(intDecimal);
        return integer + doubleDecimal;
    }

    private boolean isLowerNumber(double valueToEvaluate, double value1, double value2) {
        return  (valueToEvaluate < value1 && valueToEvaluate < value2);
    }

    private double getDecimalPart(int value){
        double result = 0;
        String strValue = "" + value;
        int numberOfDecimals = strValue.length();
        switch (numberOfDecimals){
            case 1:
                result = value * 0.1;
                break;
            case 2:
                result = value * 0.01;
                break;
        }
        return result;
    }

    private boolean validateProbability(double selectedSide, double evaluateSide1, double evaluateSide2){
        double total = selectedSide + evaluateSide1 + evaluateSide2;
        double predictPercent = (1-(selectedSide / total)) * 100;
        double predictSide1 = (1-(evaluateSide1 / total)) * 100;
        double predictSide2 = (1-(evaluateSide2 / total)) * 100;
        double differenceBetweenPredictAndSide1 = predictPercent - predictSide1;
        double differenceBetweenPredictAndSide2 = predictPercent - predictSide2;
        double totalDifference = differenceBetweenPredictAndSide1 + differenceBetweenPredictAndSide2;
        return (predictPercent >= 80 && totalDifference >= 40);
    }

    public void waitSeconds(int seconds){
        try {
            Thread.sleep(seconds * 1000);
        }catch (Exception e){
            System.out.println("error:" + e);
        }
    }
}
