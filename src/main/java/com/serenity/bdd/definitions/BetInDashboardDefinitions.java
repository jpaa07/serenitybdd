package com.serenity.bdd.definitions;

import com.serenity.bdd.steps.BetInDashboardSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.model.ExamplesTable;

public class BetInDashboardDefinitions {
    @Steps
    private BetInDashboardSteps betInDashboardSteps;

    @Given("I am going to bet the 6 events on dashboard $betData")
    public void makeNBets(ExamplesTable betData){
        betInDashboardSteps.makeNBets(betData);
    }

}
