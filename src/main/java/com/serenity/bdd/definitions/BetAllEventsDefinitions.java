package com.serenity.bdd.definitions;

import com.serenity.bdd.steps.BetAllEventsSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.model.ExamplesTable;

public class BetAllEventsDefinitions {
    @Steps
    private BetAllEventsSteps betAllEventsSteps;

    @Given("I bet in all events in playfulbet $betData")
    public void betInAllEvents(ExamplesTable betData){
        betAllEventsSteps.betInAllEvents(betData);
    }
}
