package com.serenity.bdd.definitions;

import com.serenity.bdd.steps.SearchBetSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

public class SearchBetDefinitions {

    @Steps
    private SearchBetSteps searchBetSteps;

    @Given("I am in my sccount dashboard search")
    public void validateIfDashboard(){
        searchBetSteps.validateIfDashboard();
    }

    @When("I type the team I want to bet in the search box $searchData")
    public void searchTeam(ExamplesTable searchData){
        searchBetSteps.searchTeam(searchData);
    }

    @When("select the event to bet")
    public void selectEvent(){
        searchBetSteps.selectEvent();
    }

    @When("select side to bet")
    public void selectSide(){
        searchBetSteps.selectSide();
    }

    @When("select how many coins bet to the team search $betData")
    public void makeABet(ExamplesTable betData){
        searchBetSteps.makeABet(betData);
    }

    @Then("appears confirmation message search")
    public void validateIfBet(){
        searchBetSteps.validateIfBet();
    }
}
