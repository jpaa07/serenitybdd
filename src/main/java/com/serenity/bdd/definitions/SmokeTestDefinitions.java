package com.serenity.bdd.definitions;

import com.serenity.bdd.steps.SmokeTestSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class SmokeTestDefinitions {
    @Steps
    private SmokeTestSteps smokeTestSteps;

    @Given("a system state")
    public void openPage(){
        smokeTestSteps.openPage();
    }

    @When("I do something")
    public void waitTitle(){
        smokeTestSteps.waitTitle();
    }

    @Then("system is in a different state")
    public void clickTitle(){
        smokeTestSteps.clickTitle();
    }
}
